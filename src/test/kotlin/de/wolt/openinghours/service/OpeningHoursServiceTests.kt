package de.wolt.openinghours.service

import com.fasterxml.jackson.databind.ObjectMapper
import de.wolt.openinghours.model.request.PlaceOpeningHours
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.lang.IllegalArgumentException

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
class OpeningHoursServiceTests {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var openingHoursService: OpeningHoursServiceVOne

    @Test
    fun testSingleOpeningHours() {
        val regularOpeningHours = "{\"tuesday\":[{\"type\":\"open\",\"value\":36000},{\"type\":\"close\",\"value\":64800}]}";
        val placeOpeningHours = objectMapper.readValue(regularOpeningHours, PlaceOpeningHours::class.java)
        val formatOpeningHours = openingHoursService.formatOpeningHours(placeOpeningHours)

        assertEquals("Tuesday: 10:00 AM - 6:00 PM", formatOpeningHours)
    }

    @Test
    fun testSortWeekDaysOpeningHours() {
        val regularOpeningHours = "{\"tuesday\":[{\"type\":\"open\",\"value\":36000},{\"type\":\"close\",\"value\":64800}], \n" +
                "    \"monday\":[{\"type\":\"open\",\"value\":36000},{\"type\":\"close\",\"value\":64800}]}";
        val placeOpeningHours = objectMapper.readValue(regularOpeningHours, PlaceOpeningHours::class.java)
        val formatOpeningHours = openingHoursService.formatOpeningHours(placeOpeningHours)

        assertEquals("Monday: 10:00 AM - 6:00 PM\nTuesday: 10:00 AM - 6:00 PM", formatOpeningHours)
    }

    @Test
    fun testClosingNextDayOpeningHours() {
        val regularOpeningHours = "{\"friday\":[{\"type\":\"open\",\"value\":36000}],\"saturday\":[{\"type\":\"close\",\"value\":3600}]}";
        val placeOpeningHours = objectMapper.readValue(regularOpeningHours, PlaceOpeningHours::class.java)
        val formatOpeningHours = openingHoursService.formatOpeningHours(placeOpeningHours)

        assertEquals("Friday: 10:00 AM - 1:00 AM\nSaturday: Closed", formatOpeningHours)
    }

    @Test
    fun testMultipleOpeningHoursClosingNextDayOpeningHours() {
        val regularOpeningHours = "{\"friday\":[{\"type\":\"open\",\"value\":36000}, {\"type\":\"close\",\"value\":37800}, {\"type\":\"open\",\"value\":39600}],\"saturday\":[{\"type\":\"close\",\"value\":3600}]}";
        val placeOpeningHours = objectMapper.readValue(regularOpeningHours, PlaceOpeningHours::class.java)
        val formatOpeningHours = openingHoursService.formatOpeningHours(placeOpeningHours)

        assertEquals("Friday: 10:00 AM - 10:30 AM,11:00 AM - 1:00 AM\n" +
                "Saturday: Closed", formatOpeningHours)
    }

    @Test
    fun testMultipleOpeningHours() {
        val regularOpeningHours = "{\"tuesday\":[{\"type\":\"open\",\"value\":36000},{\"type\":\"close\",\"value\":64800}, " +
                "{\"type\":\"open\",\"value\":68400},{\"type\":\"close\",\"value\":82800}]}";
        val placeOpeningHours = objectMapper.readValue(regularOpeningHours, PlaceOpeningHours::class.java)
        val formatOpeningHours = openingHoursService.formatOpeningHours(placeOpeningHours)

        assertEquals("Tuesday: 10:00 AM - 6:00 PM, 7:00 PM - 11:00 PM", formatOpeningHours)
    }

    @Test
    fun testOpeningHoursForMultipleDays() {
        val regularOpeningHours = "{\"monday\":[{\"type\":\"open\",\"value\":32400},{\"type\":\"close\",\"value\":54000}]," +
                "\"tuesday\":[{\"type\":\"open\",\"value\":36000},{\"type\":\"close\",\"value\":64800}]}"
        val placeOpeningHours = objectMapper.readValue(regularOpeningHours, PlaceOpeningHours::class.java)
        val formatOpeningHours = openingHoursService.formatOpeningHours(placeOpeningHours)

        assertEquals("Monday: 9:00 AM - 3:00 PM\nTuesday: 10:00 AM - 6:00 PM", formatOpeningHours)
    }

    @Test
    fun testInvalidOpeningHoursNoClosingHours() {
        val invalidNoCloseHours = "{\"monday\":[{\"type\":\"open\",\"value\":32400}]}"
        val placeOpeningHours = objectMapper.readValue(invalidNoCloseHours, PlaceOpeningHours::class.java)

        val e = assertThrows(IllegalArgumentException::class.java) {
            openingHoursService.formatOpeningHours(placeOpeningHours)
        }
        assertEquals("Couldn't find close time for Monday, found only opening time", e.message)
    }

}
