package de.wolt.openinghours.controler

import de.wolt.openinghours.OpeningHoursApplication
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@AutoConfigureMockMvc
@SpringBootTest(
        classes = [OpeningHoursApplication::class])
class OpeningHoursControllerTests {

    @Autowired
    private lateinit var mvc: MockMvc

    @Test
    fun testFormatRequest() {
        val fileContent = this::class.java.getResource("/full-format-test.json").readText()

        mvc.perform(post("/v1/opening-hours/format")
                .content(fileContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(content().string("Monday: Closed\n" +
                        "Tuesday: 10:00 AM - 6:00 PM\n" +
                        "Wednesday: Closed\n" +
                        "Thursday: 10:30 AM - 6:00 PM\n" +
                        "Friday: 10:00 AM - 1:00 AM\n" +
                        "Saturday: 10:00 AM - 1:00 AM\n" +
                        "Sunday: 12:00 PM - 9:00 PM"))
    }

    @Test
    fun testInvalidRequest() {
        val fileContent = "{\"monday\":[{\"type\":\"open\",\"value\":32400}]}"

        mvc.perform(post("/v1/opening-hours/format")
                .content(fileContent)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest)
                .andExpect(content().string("Couldn't find close time for Monday, found only opening time"))
    }

}
