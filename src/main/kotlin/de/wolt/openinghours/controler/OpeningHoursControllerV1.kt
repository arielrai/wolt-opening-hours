package de.wolt.openinghours.controler

import de.wolt.openinghours.model.request.PlaceOpeningHours
import de.wolt.openinghours.service.OpeningHoursServiceVOne
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Handles opening hours requests
 */
@RestController
@RequestMapping("/v1/opening-hours")
class OpeningHoursControllerV1 (val openingHoursService:OpeningHoursServiceVOne) {

    @PostMapping("/format")
    fun format(@RequestBody placeOpeningHours: PlaceOpeningHours): String {
        return openingHoursService.formatOpeningHours(placeOpeningHours);
    }
}