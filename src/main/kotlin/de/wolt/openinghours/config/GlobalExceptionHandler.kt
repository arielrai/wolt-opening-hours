package de.wolt.openinghours.config

import org.springframework.core.Ordered
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.lang.IllegalArgumentException
import org.springframework.http.ResponseEntity
import org.slf4j.LoggerFactory
import org.springframework.core.annotation.Order
import org.springframework.web.bind.annotation.ExceptionHandler

/**
 * Configures the responses for every type of exception
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
class GlobalExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(IllegalArgumentException::class)
    fun illegalArgumentHandler(ex: IllegalArgumentException): ResponseEntity<*> {
        Companion.logger.error(ex.message, ex)
        return ResponseEntity.badRequest().body(ex.message)
    }

    companion object {
        private val logger = LoggerFactory.getLogger(GlobalExceptionHandler::class.java)
    }
}