package de.wolt.openinghours.model.request

data class OpeningHours(val type: OpeningHoursType, val value: Long)