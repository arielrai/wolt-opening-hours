package de.wolt.openinghours.model.request

import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonRootName
import java.time.DayOfWeek
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

data class PlaceOpeningHours (
    val root: MutableMap<DayOfWeek, List<OpeningHours>> = sortedMapOf<DayOfWeek, List<OpeningHours>>()) {

    @JsonAnySetter
    public fun ignored(name: String, value: ArrayList<OpeningHours>) {
        if (value.isEmpty()) {
            root[DayOfWeek.valueOf(name.toUpperCase())] = Collections.emptyList<OpeningHours>()
        } else {
            root[DayOfWeek.valueOf(name.toUpperCase())] = value
        }
    }

}