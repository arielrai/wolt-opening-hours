package de.wolt.openinghours.model.request

enum class OpeningHoursType(val openHoursTitle: String) {
    OPEN("Open"), CLOSE("Closed")
}