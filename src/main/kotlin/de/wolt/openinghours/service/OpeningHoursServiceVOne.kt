package de.wolt.openinghours.service

import de.wolt.openinghours.model.request.OpeningHours
import de.wolt.openinghours.model.request.OpeningHoursType
import de.wolt.openinghours.model.request.PlaceOpeningHours
import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Service
import java.lang.IllegalArgumentException
import java.time.DayOfWeek
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * Formats PlaceOpeningHours to a human-readable string
 */
//FIXME I would normally never name it ServiceNameVOne, instead I'd name it ServiceNameV1
// But for some reason intellij is not recognizing this class with V1 in the name, it's probably a bug
@Service
class OpeningHoursServiceVOne {

    private val dtf = DateTimeFormatter.ofPattern("h:mm a").localizedBy(Locale.US) // US locale to have AM/PM in uppercase

    fun formatOpeningHours(placeOpeningHours: PlaceOpeningHours): String {
        val sb = StringBuilder()

        placeOpeningHours.root.forEach { (dayOfWeek, openingHours) ->
            if (openingHours.isEmpty()) {
                if (sb.isNotEmpty())
                    sb.append("\n")
                sb.append(formatOpeningHours(dayOfWeek, OpeningHoursType.CLOSE.openHoursTitle))
            } else {
                val dayOfWeekFormat = StringBuilder()
                var openingTime: Long? = null;

                openingHours.forEach { (type, value) ->
                    run {
                        if (type == OpeningHoursType.OPEN) {
                            openingTime = value;
                        } else {
                            if (openingTime != null) {
                                if (dayOfWeekFormat.isNotEmpty()) {
                                    dayOfWeekFormat.append(", ")
                                }
                                dayOfWeekFormat.append(formatOpenClose(openingTime!!, value))
                                openingTime = null;
                            }
                        }
                    }
                }
                if (openingTime != null) {
                    val closeTime: OpeningHours = placeOpeningHours.root[dayOfWeek.plus(1)].let {
                        if (it != null && it.isNotEmpty()) {
                            it[0];
                        } else {
                            throw IllegalArgumentException(String.format("Couldn't find close time for %s, found only opening time", dayOfWeekToString(dayOfWeek)))                        }
                    }
                    if (dayOfWeekFormat.isNotEmpty()) {
                        dayOfWeekFormat.append(",")
                    }
                    dayOfWeekFormat.append(formatOpenClose(openingTime!!, closeTime.value))
                } else {
                    if (dayOfWeekFormat.isEmpty()) {
                        dayOfWeekFormat.append(OpeningHoursType.CLOSE.openHoursTitle)
                    }
                }
                if (sb.isNotEmpty())
                    sb.append("\n")
                sb.append(formatOpeningHours(dayOfWeek,
                        dayOfWeekFormat.toString()))
            }
        }
        return sb.toString()
    }

    private fun formatOpenClose(openTime: Long, closeTime: Long): String {
        LocalTime.ofSecondOfDay(openTime).format(dtf)

        return String.format("%s - %s", LocalTime.ofSecondOfDay(openTime).format(dtf),
                LocalTime.ofSecondOfDay(closeTime).format(dtf));
    }

    private fun dayOfWeekToString(dayOfWeek: DayOfWeek): String {
        return StringUtils.capitalize(dayOfWeek.name.toLowerCase());
    }

    private fun formatOpeningHours(dayOfWeek: DayOfWeek, message: String): String {
        return String.format("%s: %s", dayOfWeekToString(dayOfWeek), message);
    }

}