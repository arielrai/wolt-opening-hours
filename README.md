[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=wolt-opening-hours&metric=alert_status)](https://sonarcloud.io/dashboard?id=wolt-opening-hours)

## Open Questions/Points

- Should the request contain all the days of the week? If so, some validation for that case could be added. Rename
- OpeningHoursServiceVOne to OpeningHoursServiceV1. This hasn't been done because intellij is not recognizing the class in
that case. Probably a bug 
- Dockerize application

## Local Setup IDE

Clone and open the project in your IDE. Run the main
class [OpeningHoursApplication](src/main/kotlin/de/wolt/openinghours/OpeningHoursApplication.kt) as spring boot
application.

The Opening Hours API is now ready to receive requests!

## Local Setup terminal

- <code>./gradlew build</code>
- <code>java -jar ./build/libs/opening-hours-0.0.1-SNAPSHOT.jar</code>

The Opening Hours API is now ready to receive requests!

# PART 1

POST http://localhost:8080/v1/opening-hours/format
Content-Type: application/json

```json
{
  "monday": [],
  "tuesday": [
    {
      "type": "open",
      "value": 36000
    },
    {
      "type": "close",
      "value": 64800
    }
  ],
  "wednesday": [
  ],
  "thursday": [
    {
      "type": "open",
      "value": 37800
    },
    {
      "type": "close",
      "value": 64800
    }
  ],
  "friday": [
    {
      "type": "open",
      "value": 36000
    }
  ],
  "saturday": [
    {
      "type": "close",
      "value": 3600
    },
    {
      "type": "open",
      "value": 36000
    }
  ],
  "sunday": [
    {
      "type": "close",
      "value": 3600
    },
    {
      "type": "open",
      "value": 43200
    },
    {
      "type": "close",
      "value": 75600
    }
  ]
}
```

Response

```code
Monday: Closed
Tuesday: 10:00 AM - 6:00 PM
Wednesday: Closed
Thursday: 10:30 AM - 6:00 PM
Friday: 10:00 AM - 1:00 AM
Saturday: 10:00 AM - 1:00 AM
Sunday: 12:00 PM - 9:00 PM
```

# PART 2

The format of the data <b>was not</b> optimal due to the following reasons:

- Closing hours belonging to next day in a few cases, makes data hard to process and read. Opening and closing hours should've
  been kept in the same object
- Transform day of the week into variable in json object. This would reduce the number of parent nodes with different names,
  standardizing the structure of the days as well as its key and facilitate navigability over the json object e.g.:

```json
{
  "day_of_week": "monday",
  "opening_hours": [{
    "type": "open",
    "value": 3600
  },
  {
    "type": "close",
    "value": 36000
  }]
}
```
The request would then look like this:

```json
{
  "week_opening_hours": [{
      "day_of_week": "monday",
      "opening_hours": [
        {
          "type": "open",
          "value": 3600
        },
        {
          "type": "close",
          "value": 36000
        }
      ]},
      {
      "day_of_week": "tuesday",
      "opening_hours": [
        {
          "type": "open",
          "value": 3600
        },
        {
          "type": "close",
          "value": 36000
        }
      ]}
  ]}
```

- Change weekdays to numbers from 0-6. This would reduce the size of the request, make the request non-dependent of english.
- The values for opening hours could be changed from seconds to 0000–2359 values, making the request more human-readable.